//
//  ViewController.swift
//  CoinflipATMPartnerDemoApp
//
//  Created by Husein Kareem on 1/19/22.
//

import UIKit
import CoinflipATMPartnerSDKiOS

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func didTapLaunchSDKButton(_ sender: Any) {
        CoinflipATMPartnerSDKiOS.shared.userLocationCoordinates = (41.8781, -87.6298)
        CoinflipATMPartnerSDKiOS.shared.launchSDK(fromViewController: self, completion: nil)
    }
}

