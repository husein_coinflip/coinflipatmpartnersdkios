Pod::Spec.new do |s|
  s.name          = "CoinflipATMPartnerSDKiOS"
  s.version       = "0.0.1"
  s.summary       = "The CoinFlip ATM Partner SDK allows you to view our ATM locations to purchase Bitcoin, Ethereum and more with cash instantly on all iOS platforms."
  s.description   = "The CoinFlip ATM Partner SDK allows you to view our ATM locations to purchase Bitcoin, Ethereum and more with cash instantly on all iOS platforms."
  s.homepage      = "https://gitlab.com/husein_coinflip/coinflipatmpartnersdkios"
  s.license       = "LICENSE.md"
  s.author        = { "Husein Kareem" => "hkareem@coinflip.tech" }
  s.platform      = :ios, "15.2"
  s.swift_version = "5.0"
  s.source        = {
    :git => "https://gitlab.com/husein_coinflip/coinflipatmpartnersdkios",
    :tag => "#{s.version}"
  }
  s.source_files        = "CoinflipATMPartnerSDKiOS/**/*.{h,m,swift}"
  s.public_header_files = "CoinflipATMPartnerSDKiOS/**/*.h"
end