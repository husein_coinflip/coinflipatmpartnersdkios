//
//  CoinflipATMPartnerSDKiOS.swift
//  CoinflipATMPartnerSDKiOS
//
//  Created by Husein Kareem on 1/19/22.
//

import Foundation
import UIKit

/**
 *  The primary interface for interacting with the partner SDK.
 */

public class CoinflipATMPartnerSDKiOS {
    
    public static let shared = CoinflipATMPartnerSDKiOS()
    public var userLocationCoordinates: (latitude: Double, longitude: Double) = (latitude: 0.00, longitude: 0.00)
    public var sdkAPIKey: String = ""
    
    let helloWorld = "HelloWorld"
    
    public init() {}
    
    /**
     Launches the SDK's UI from a given view controller as a modal.
     
     - parameter viewController: The view controller which you want to present the UI for getting a space through Coinflip
     - parameter completion:     A completion block to be passed through to `pushViewController`, or nil. Defaults to nil.
     */
    public func launchSDK(fromViewController viewController: UIViewController? = nil,
                          completion: ((UIViewController?) -> Void)? = nil) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle(for: CoinflipATMPartnerSDKiOS.self))
        guard let initialViewController = storyboard.instantiateInitialViewController() else {
            return
        }
        
        viewController?.present(initialViewController, animated: true, completion: {
            completion?(nil)
        })
    }
}
