//
//  ViewController.swift
//  CoinflipATMPartnerSDKiOS
//
//  Created by Husein Kareem on 1/19/22.
//

import UIKit
import MapKit

protocol HandleMapSearch {
    
    func dropPinZoomIn(placemark: MKPlacemark)
    
}

class ViewController: UIViewController {
    
    @IBOutlet private weak var atmDetailsView: UIView!
    @IBOutlet private weak var atmDetailsTitleLabel: UILabel!
    @IBOutlet private weak var atmDetailsSubtitleLabel: UILabel!
    @IBOutlet private weak var mapView: MKMapView!
    
    private var resultsSearchController: UISearchController? = nil
    private var selectedPin: MKPlacemark? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.atmDetailsView.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.mapSetup()
        
        guard let locationSearchResultsTableViewController = self.storyboard?.instantiateViewController(withIdentifier: "LocationSearchResultsTableViewController") as? LocationSearchResultsTableViewController else {
            return
        }
        
        self.resultsSearchController = UISearchController(searchResultsController: locationSearchResultsTableViewController)
        self.resultsSearchController?.searchResultsUpdater = locationSearchResultsTableViewController
        self.resultsSearchController?.searchBar.delegate = self
        let searchBar = self.resultsSearchController!.searchBar
        searchBar.sizeToFit()
        searchBar.placeholder = "Search nearby"
        self.navigationItem.searchController = self.resultsSearchController
        self.resultsSearchController?.hidesNavigationBarDuringPresentation = true
        self.definesPresentationContext = true
        locationSearchResultsTableViewController.mapView = self.mapView
        locationSearchResultsTableViewController.handleMapSearchDelegate = self
    }
    
    private func mapSetup() {
        let coordinate = CLLocationCoordinate2D(latitude: CoinflipATMPartnerSDKiOS.shared.userLocationCoordinates.latitude, longitude: CoinflipATMPartnerSDKiOS.shared.userLocationCoordinates.longitude)
        let region = self.mapView.regionThatFits(MKCoordinateRegion(center: coordinate, latitudinalMeters: 4000, longitudinalMeters: 4000))
        self.mapView.setRegion(region, animated: true)
        self.mapView.removeAnnotations(self.mapView.annotations)
        self.mapView.delegate = self
//        self.mapView.register(ATMAnnotationView.self, forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
    }
    
    private func fetchATMLocations(with searchCoordinate: CLLocationCoordinate2D) -> [MKPointAnnotation] {
        let atmAnnotation1 = MKPointAnnotation()
        atmAnnotation1.coordinate = CLLocationCoordinate2D(latitude: searchCoordinate.latitude + 0.01,
                                                           longitude: searchCoordinate.longitude - 0.01)
        atmAnnotation1.title = "Atm Location 1"
        
        let atmAnnotation2 = MKPointAnnotation()
        atmAnnotation2.coordinate = CLLocationCoordinate2D(latitude: searchCoordinate.latitude + 0.01,
                                                           longitude: searchCoordinate.longitude - 0.02)
        atmAnnotation2.title = "Atm Location 2"
        
        let atmAnnotation3 = MKPointAnnotation()
        atmAnnotation3.coordinate = CLLocationCoordinate2D(latitude: searchCoordinate.latitude + 0.02,
                                                           longitude: searchCoordinate.longitude - 0.01)
        atmAnnotation3.title = "Atm Location 3"
        
        return [atmAnnotation1,
                atmAnnotation2,
                atmAnnotation3]
    }

}

extension ViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        self.atmDetailsView.isHidden = false
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        self.atmDetailsView.isHidden = true
    }
    
//    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
//        return mapView.dequeueReusableAnnotationView(withIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier, for: annotation)
//    }
}

extension ViewController: HandleMapSearch {
    
    func dropPinZoomIn(placemark: MKPlacemark) {
        self.selectedPin = placemark
        self.mapView.removeAnnotations(self.mapView.annotations)
        let annotation = MKPointAnnotation()
        annotation.coordinate = placemark.coordinate
        annotation.title = placemark.name
        let atmLocations = self.fetchATMLocations(with: annotation.coordinate)
        self.mapView.addAnnotations(atmLocations)
        self.mapView.addAnnotation(annotation)
        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
        let region = MKCoordinateRegion(center: placemark.coordinate, span: span)
        self.mapView.setRegion(region, animated: true)
    }
}

extension ViewController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.mapSetup()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            self.searchBarCancelButtonClicked(searchBar)
        }
    }
}
