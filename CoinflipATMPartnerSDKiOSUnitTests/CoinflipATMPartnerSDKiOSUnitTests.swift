//
//  CoinflipATMPartnerSDKiOSUnitTests.swift
//  CoinflipATMPartnerSDKiOSUnitTests
//
//  Created by Husein Kareem on 1/19/22.
//

import XCTest
@testable import CoinflipATMPartnerSDKiOS

class CoinflipATMPartnerSDKiOSUnitTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testhelloWorld() throws {
        let hw = CoinflipATMPartnerSDKiOS()
        XCTAssertEqual(hw.hello(to: "World"), "Hello World")
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        measure {
            // Put the code you want to measure the time of here.
        }
    }

}
