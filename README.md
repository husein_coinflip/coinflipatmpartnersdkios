# CoinflipATMPartnerSDKiOS
 
The CoinFlip ATM Partner SDK allows you to view our ATM locations to purchase Bitcoin, Ethereum and more with cash instantly on all iOS platforms.

## License

Please read LICENSE.md for our terms. ™️©👨🏻‍⚖️

## Before You Start

To add the CoinflipATMPartnerSDKiOS to your application, you will need a Partner Key.

## Getting Started

Use [CocoaPods](https://guides.cocoapods.org/using/getting-started.html) to install our SDK. Follow the guide at the link to get started if you do not already have CocoaPods installed.

Add the following line to your [Podfile](https://guides.cocoapods.org/using/the-podfile.html), within the target you wish to add our SDK to:

```ruby
target 'YourAppTargetName'
  pod 'CoinflipATMPartnerSDKiOS', '~> 0.0.1'
end
```

**NOTE**: Since our SDK is in Swift, you _must_ use the [`use_frameworks!`](https://guides.cocoapods.org/syntax/podfile.html#use_frameworks_bang) flag in your Podfile, or it won't build.

Using the terminal, `cd` into the directory where your `Podfile` is located.  Run `pod install`, and the current version of our SDK will be installed.

Make sure you follow CocoaPods' instructions and open the `.xcworkspace` file instead of the `.xcodeproj` file, or your code won't build.

## Using The SDK in your Code

To use the SDK or its elements in a Swift file, add the following line to the top of your file:

```swift
import CoinflipATMPartnerSDKiOS
```

To use the SDK or its elements in Objective-C file, add this line to the top of your file instead:

```objectivec
@import CoinflipATMPartnerSDKiOS;
```
### Launching the SDK

The CoinflipATMPartnerSDKiOS is implemented as a singleton which can be launched from any `UIViewController` subclass. It will be presented modally. There is only one **required** property which must be set:

The absolute bare minimum implementation, assuming you have a button hooked up to this IBAction, is as follows:

```swift
    
    /**
     Launches the SDK's UI from a given view controller as a modal.
     
     - parameter viewController: The view controller which you want to present the UI for getting a space through Coinflip
     - parameter completion:     A completion block to be passed through to `pushViewController`, or nil. Defaults to nil.
     */
    public func launchSDK(fromViewController viewController: UIViewController? = nil,
                          completion: ((UIViewController?) -> Void)? = nil) {...}
```

### Sample App Screenshots
<img src="READMEImages/screenshot1.png" alt="Screenshot 1" width="250"/>
<img src="READMEImages/screenshot2.png" alt="Screenshot 2" width="250"/>
<img src="READMEImages/screenshot3.png" alt="Screenshot 3" width="250"/>
<img src="READMEImages/screenshot4.png" alt="Screenshot 4" width="250"/>
<img src="READMEImages/screenshot5.png" alt="Screenshot 5" width="250"/>
